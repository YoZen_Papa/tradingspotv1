<?php

namespace App\Http\Controllers;
use App\Add;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class AddsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use UploadTrait;
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['index']]);
    }

    public function pagination(Request $request) 
    {
        if ($request->cat_id) {
            $adds = Add::with('categories')->where('category_id', $request->cat_id)->orderBy('updated_at', 'DESC')->paginate(2);
        } else {
            $adds = Add::with('categories')->orderBy('updated_at', 'DESC')->paginate(2);
        }
        foreach ($adds as $add) {
            $add->user->load('postalCode');
        }    
        return $adds;
    }

    public function index()
    {
        $adds = Add::with('categories')->orderBy('updated_at', 'DESC')->get();
        $adds->load('user');
        return $adds;
        // return the adds, sorted by when it was last updated
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required', 'description' => 'required', 'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8000']);
        // validate the request
        $add = new Add();
        // set a new instance of the add class
        $add->title = request('title');
        $add->description = request('description');
        // set the title and description properties
        if ($request->has('image') && $request->image != null) {
            $image = $request->file('image');
            $name = Str::slug($request->input('title')).'_'.time();
            $folder = '/uploads/images/';
            $filepath = $folder . $name. '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
            $add->image = '/storage' . $filepath;
        } else if ($request->image == null){
            $add->image = null;
        }
        // sets the image to null if no file was submitted, otherwise makes it so the image will form a direct path to the folder that the image gets stored in
        $add->sold_status = FALSE;
        // default sold status is false
        $add->user_id = request('user_id');
        // set the user property to know who owns the add
        $add->save();
        // save the add
        $add->categories()->attach($request->get('category_id'));
        // attach the categories selected in the multiselect
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Add $add)
    {
        // $add->with('user');
        $add->load('user');
        $add->load('categories');
        return $add;
        // load both the user of the add and the categories, then return the add
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Add $add)
    {
        $add->load('user');
        $add->load('categories');
        return $add;
        // load both the user of the add and the categories, then return the add
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Add $add)
    {
        $this->validate($request, ['title' => 'required', 'description' => 'required', 'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8000']);
        // validate the request
        $add = new Add();
        // set a new instance of the add class
        $add->title = request('title');
        $add->description = request('description');
        // set the title and description properties
        if ($request->has('image') && $request->image != null) {
            $image = $request->file('image');
            $name = Str::slug($request->input('title')).'_'.time();
            $folder = '/uploads/images/';
            $filepath = $folder . $name. '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
            $add->image = '/storage' . $filepath;
        } else if ($request->image == null){
            $add->image = null;
        }
        // sets the image to null if no file was submitted, otherwise makes it so the image will form a direct path to the folder that the image gets stored in
        $add->user_id = request('user_id');
        // set the user property to know who owns the add
        $add->save();
        // save the add
        $add->categories()->attach($request->get('category_id'));
        // attach the categories selected in the multiselect
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Add $add)
    {
        $add->delete();
        // delete the add
    }
}
