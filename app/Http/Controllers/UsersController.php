<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PostalCode;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return $users;
        // return all the users
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required', 'password' => 'required', 'postalCode' => 'required']);
        $user = new User();
        // make a new user instance.
        $user->name = request('name');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));
        $user->username = request('username');
        $postcode = PostalCode::where('postcode', request('postalCode'))->get();
        if (count($postcode) == 1) {
            foreach ($postcode as $code) {
                $user->postalcode_id = $code->id;
            }
        } else {alert('something went wrong');}
        // set the data.
        $user->save();
        //save the user.
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required', 'password' => 'required', 'postalCode' => 'required']);
        $user = new User();
        // make a new user instance.
        $user->name = request('name');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));
        $user->username = request('username');
        $user->postalCode = request('postalCode');
        // set the data.
        $user->save();
        //save the user.
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user-delete();
        // delete the user.
    }
}
