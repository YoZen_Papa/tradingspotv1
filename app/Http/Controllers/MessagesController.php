<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use App\Message;

class MessageController extends Controller
{
    public function __construct()
    {
         // $this->middleware('auth:api', ['except' => ['index']]);
    }

    public function index()
    {
        $messages = Message::with('user')->get();
        return $messages;
    }

    public function fetchMessages()
    {
        return Message::with('user')->get();
    }

    public function sendMessage(Request $request)
    {
        user()->messages()->create([
            'message' => $request->message
        ]);
        return ['status' => 'success'];
    }
}
