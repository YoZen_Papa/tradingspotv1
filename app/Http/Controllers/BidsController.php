<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bid;

class BidsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bids = Bid::all();
        return $bids;
        // return all the bids
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['amount' => 'required', 'user_id' => 'required', 'add_id' => 'required']);
        // validate the request
        $bid = new Bid();
        // create a new instance of the bid class
        $bid->amount = request('amount');
        $bid->user_id = request('user_id');
        $bid->add_id = request('add_id');
        // set properties
        $bid->save();
        // save the bid
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Bid $bid)
    {
        $bid->load('user');
        $bid->load('add');
        return $bid;
        // load both the user and the add with the bid, then return it
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Bid $bid)
    {
        $bid->load('user');
        $bid->load('add');
        return $bid;
        // load both the user and the add with the bid, then return it
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bid $bid)
    {
        $this->validate($request, ['amount' => 'required', 'user_id' => 'required', 'add_id' => 'required']);
        // validate the request
        $bid = new Bid();
        // set a new instance fo the bid class
        $bid->amount = request('amount');
        $bid->user_id = request('user_id');
        $bid->add_id = request('add_id');
        // set the properties
        $bid->save();
        // save the bid
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bid $bid)
    {
        $bid->delete();
        // delete the bid
    }
}
