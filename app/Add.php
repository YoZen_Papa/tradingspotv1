<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bid;
use App\User;
use App\Category;
class Add extends Model
{
    public function user () {
        return $this->belongsTo(User::class);
    }
    public function categories() {
        return $this->belongsToMany(Category::class);
    }
    public function getImagePathAttribute() {
        return asset('/storage' + $this->image);
      
    }
    public function bid () {
        return $this->hasmany(Bid::class);
    }
}
