<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\AddsController;
use App\Http\Controllers\BidsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('users', UsersController::class);
Route::resource('categories', CategoriesController::class);
Route::resource('adds', AddsController::class);
Route::resource('bids', BidsController::class);
use App\Http\Controllers\AuthController;
Route::post('adds/pagination', [AddsController::class, 'pagination']);
Route::group([
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::get('me', [AuthController::class, 'me']);
});
Route::get('/', function () {
    broadcast(new DummyEvent('some data'));
});
// Route::post('/messages', MessagesController::class, 'sendMessage');
// Route::get('/messages/index', MessagesController::class, 'index');
// Route::get('/messages', MessagesController::class, 'fetchMessages');
use App\Category;
use App\Add;
use App\Bid;
use App\User;
use App\Message;
use App\Http\Controllers\MessageController;
