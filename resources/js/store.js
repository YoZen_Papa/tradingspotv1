import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        users: [],
        categories: [],
        adds: [],
        bids: [],
        current_user: null,
        temp_adds: [],
        pagination: {},
    },
    mutations: {
        loginUserMutation(state, data) {
            state = state
            // not changing the state at all
            localStorage.setItem('token', data.data.access_token)
            // sets the access token in local storage for later use
        },
        logoutUserMutation(state) {
            state.current_user = {}
            // sets the current user to an empty object
            localStorage.removeItem('token')
            // removes the access token in local storage
        },
        fetchUserMutation(state, data) {
            state.current_user = data.data
            // sets the current user to be equal to the profile data accepted from the request
        },
        registerUserMutation(state, data) {
            state.users = data
            // sets the users in the state equal to the list of users accepted from the request
        },
        refreshUserMutation(state, data) {
            state = state
            // not changing the state at all
            localStorage.setitem('token', data.access_token)
            // sets the access token in local storage again, for later use
        },
        editUserMutation(state, data) {
            state.users.splice(state.users.indexOf(data.data), 1, data.data)
            // replaces the old user in the state with the patched one from the request data
        },
        createAddMutation(state, data) {
            state.adds = data
            // sets the adds in the state equal to the list of adds accepted from the request
        },
        createBidMutation(state, data) {
            state.bids = data
            // sets the bids in the state equal to the list of bids accepted from the request
        },
        deleteBidMutation(state, data) {
            state.bids.splice(state.bids.indexOf(data.data), 1)
            // deletes the bid that was deleted from the list of bids in the state
        },
        getAllBidsMutation(state, data) {
            state.bids = data.data
            // sets the bids in the state equal to the list of bids accepted from the request
        },
        editBidMutation(state, data) {
            state.bids.splice(state.bids.indexOf(data.data), 1, data.data)
            // replaces the old bid in the state with the patched one from the request data
        },
        editAddMutation(state, data) {
            state.adds.splice(state.adds.indexOf(data.data), 1, data.data)
            // replaces the old add in the state with the patched one from the request data
        },
        deleteAddMutation(state, data) {
            state.adds.splice(state.adds.indexOf(data), 1)
            // deletes the add that was deleted from the list of adds in the state
        },
        getAllAddsMutation(state, data) {
            state.adds = data.data
            state.pagination = data
            // sets the adds in the state equal to the list of adds accepted from the request
        },
        getAllCategoriesMutation(state, data) {
            state.categories = data.data
            // sets the categories in the state equal to the list of categories accepted from the request
        },
        getAllBidsMutation(state, data) {
            state.bids = data.data
            // sets the list of bids in the state equal to the list of categories accepted from the request
        },
        fetchAddMutation(state, data) {
            state.temp_adds = [data.data]
            // sets the temp_adds equal to an array containing the data accepted from the request, has to specifically be an array
        },
    },
    actions: {
        fetchUserAction(context) {
            return new Promise((resolve,reject) => {
                axios.get('api/auth/me')
                // sends a request
                .then(response => {
                    context.commit('fetchUserMutation', response)
                    resolve(response)
                    // accepts the response, then fires the mutation
                })
                .catch(errors => {
                    console.log(errors.response.statusText)
                    reject(errors)
                    // catches any error messages and logs them
                })
            })
        },
        loginUserAction(context, formData) {            
            return axios.post(`api/auth/login`, {
                    email: formData.email,
                    password: formData.password
            })
            // sends a request, specifies the properties with the request to prevent extra data from being sent
            .then(data => {
                context.commit('loginUserMutation', data)
                // accepts the data, then fires the mutation
            })
            
            .catch(error => {
                console.log(error.response.statusText)
                // catches any error messages and logs them
            })
            
        },
        logoutUserAction(context) {
            axios.post('api/auth/logout')
            // sends a request
            .then(data => {
                context.commit('logoutUserMutation', data)
                // accepts the data, then fires the mutation
            })
            
            .catch(error => {
                console.log(error)
                // catches any error messages and logs them
            })
            
        },
        refreshUserAction(context) {
            return new Promise((resolve, reject) => {
                axios.post('api/auth/refresh')
                // sends a request
                .then(data => {
                    context.commit('refreshUserMutation', data)
                    resolve(data)
                    // accepts the data, then fires the mutation
                })
                .catch(error => {
                    console.log(error.response.statusText)
                    reject(error)
                    // catches any error messages and logs them
                })
            })
        },
        registerUserAction(context, formData) {
            return axios.post('api/users', formData)
            // sends a request
            .then(data => {
                context.commit('registerUserMutation', data);
                // accepts the data, then fires the mutation
            })
            .catch(error => {
                console.log(error);
                // catches any error messages and logs them
            })
            
        },
        editUserAction(context, payload) {
            return new Promise((resolve, reject) => {
                axios.patch(`api/users/${payload.id}`, payload.formData)
                // sends the patch request
                axios.get(`api/users/${payload.id}`)
                // sends a request to get the recently patched user
                .then(data => {
                    context.commit('editUserMutation', data)
                    resolve(data)
                    // accepts the data, then fires the mutation
                })
                .catch(error => {
                    console.log(error.response.statusText);
                    reject(error)
                    // catches any error messages and logs them
                })
            })
        },
        createBidAction(context, payload) {
            return new Promise((resolve, reject) => {
                axios.post('api/bids', payload)
                // sends a request to post the bid
                axios.get('api/bids')
                // sends a request to get the list of all bids
                .then(({data}) => {
                    context.commit('createBidMutation', data)
                    resolve(data)
                    // accepts the data, then fires the mutation
                })
                .catch(error => {
                    console.log(error.response.statusText)
                    reject(errort)
                    // catches any error messages and logs them
                })
            })
        },
        deleteBidAction(context, payload) {
            return new Promise((resolve, reject) => {
                axios.post(`api/bids/${payload.id}`, {'_method': 'DELETE'})
                // sends a request, specifying the delete method
                .then(
                    context.commit('deleteBidMutation', payload),
                    resolve(payload)
                    // fires the mutation
                )
                .catch(error => {
                    console.log(error.response.statusText)
                    reject(error)
                    // catches any error messages and logs them
                })
            })
        },
        editBidAction(context, payload) {
            return new Promise((resolve, reject) => {
                axios.patch(`api/bids/${payload.id}`, payload.formData)
                // sends a request that patches the bid
                axios.get(`api/bids/${payload.id}`)
                // sends a request that gets the recently patched bid
                .then(data => {
                    context.commit('editBidMutation', data)
                    resolve(data)
                    // accepts the data, then fires the mutation
                })
                .catch(error => {
                    console.log(error.response.statusText);
                    reject(error)
                    // catches any error messages and logs them
                })
            })
        },
        getAllBidsAction(context) {
            return axios.get('api/bids')
            // sends a request to get all bids
            .then(data => {
                context.commit('getAllBidsMutation', data)
                // accepts the data and fires the mutation
            })
            .catch(error => {
                console.log(error.response.statusText)
                // catches any error messages and logs them
            })
        },
        createAddAction(context, payload) {
            return new Promise((resolve, reject) => {
                let config = {
                    headers: {'content-type': 'multipart/form-data'}
                }
                // defines config to set the content type equal to multipart or form data in the upcoming request, needed so that your post request can contain both formdata and file data for the image
                axios.post('api/adds', payload, config)
                // sends a request that makes the add, using the config to make sure the image gets stored correctly
                axios.get('api/adds')
                // sends a request to get all adds
                .then(({data}) => {
                    context.commit('createAddMutation', data)
                    resolve(data)
                    // accepts the data and fires the mutation
                })
                .catch(error => {
                    console.log(error);
                    reject(error.response.statusText)
                    // catches any error messages and logs them
                })
            })
        },
        deleteAddAction(context, payload) {
            return new Promise((resolve, reject) => {
                axios.post(`api/adds/${payload.id}`, {'_method': 'DELETE'})
                // sends a request, specifying the delete method
                .then(
                    context.commit('deleteAddMutation', payload),
                    resolve(payload)
                    // fires the mutation
                    )
                .catch(error => {
                    console.log(error.response.statusText)
                    reject(error)
                    // catches any error messages and logs them
                })
            })
        },
        editAddAction(context, payload) {
            return new Promise((resolve, reject) => {
                axios.patch(`api/adds/${payload.id}`, payload.formData)
                // sends a request to patch the add
                axios.get(`api/adds/${payload.id}`)
                // sends a request to get the recently patched add
                .then(data => {
                    context.commit('editAddMutation', data)
                    resolve(data)
                    // accepts the data and fires the mutation
                })
                .catch(error => {
                    console.log(error.response.statusText)
                    reject(error)
                    // catches any error messages and logs them
                })
            })
        },
        getAllAddsAction(context) {
            return axios.get('api/adds')
            // sends a request to get all adds
            .then(response => {
                context.commit('getAllAddsMutation', response.data)
                // fires the mutation
            })
            .catch(error => {
                console.log(error.response.statusText)
                // catches any error messages and logs them
            })
            // axios.get('api/add_category').then(data => {
                //     x = data.data
                // })
                // .catch(error => {
                //     console.log(error.response.statusText);
                // })
                // addWithData = data.data + x
        },
        getAllCategoriesAction(context) {
            return new Promise((resolve, reject) => {
                axios.get('api/categories')
                // sends a request to get all categories
                .then(data => {
                    context.commit('getAllCategoriesMutation', data)
                    resolve(data)
                    // accepts the data and fires the mutation
                })
                .catch(error => {
                    console.log(error.response.statusText)
                    reject(error)
                    // catches any error messages and logs them
                })
            })
        },
        getAllBidsAction(context) {
            return axios.get('api/bids')
            // sends a request to get all bids
            .then(data => {
                context.commit('getAllBidsMutation', data)
                // accepts the data and fires the mutation
            })
            .catch(error => {
                console.log(error.response.statusText)
                // catches any error messages and logs them
            })
        },
        fetchAddAction(context, id) {
            return new Promise((resolve, reject) => {
                axios.get(`api/adds/${id}`)
                // sends a request to get a specific add based on the id
                .then(data => {
                    context.commit('fetchAddMutation', data)
                    resolve(data)
                    // accepts the data and fires the mutation
                })
                .catch(error => {
                    console.log(error.response.statusText)
                    reject(error)
                    // catches any error messages and logs them
                })
            })
        },
    },
    getters: {
        getAddsFromLoggedInUser: (state, getters) => {
            return getters.authIsAuth ? state.adds.filter((add) => {
                return add.user_id == state.current_user.id
            }) : [];
            // filters the list of adds in the state, returns the adds that belong to the user currently logged in if they are authenticated, otherwise returns an empty array
        },
        authIsAuth: (state) => {
            return !!state.current_user;
            // returns true if there is a user, returns false if there is no user
        },
        getBidsFromLoggedInUser: (state, getters) => {
            return getters.authIsAuth ? state.bids.filter((bid) => {
                return bid.user_id == state.current_user.id
            }): [];
            // filters the list of bids in the state, returns the bids that belong to the user currently logged in if they are authenticated, otherwise returns an empty array
        },
        getBidsByID: (state) => (addID) => {
            return state.bids.filter(function(bid) {
                return bid.add_id == addID
            })
            // filters the list of bids in the state to find the bids that belong to a certain add and returns those
        },
        getSingleAdd: (state) => {
            return state.temp_adds.length > 0 ? state.temp_adds[0] : false;
            // if there is any one thing or more in temp_adds, returns the first thing in it, otherwise returns false
        }
    }
})