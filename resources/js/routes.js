import VueRouter from 'vue-router';
import store from './store';


let routes = [
    {
        path: '/',
        name: 'homepage',
        component: require('./components/homepageComponent').default,
        meta: {public: true, layout: 'standard'}
    },
    {
        path: '/users/login',
        name: 'users.login',
        component: require('./components/loginComponent').default,
        meta: {public: true, layout: 'standard'}
    },
    {
        path: '/users/logout',
        name: 'users.logout',
        component: require('./components/LogoutComponent').default,
        meta: {public: true, layout: 'standard'}
    },
    {
        path:'/users/register',
        name: 'users.register',
        component: require('./components/RegisterUserComponent').default,
        meta: {public: true, layout: 'standard'}
    },
    {
        path: '/users/profile',
        name: 'users.profile',
        component: require('./components/profilepageComponent').default,
        meta: {public: true, layout: 'standard'}
    },
    {
        path: '/adds/create',
        name: 'adds.create',
        component: require('./components/createAddComponent').default,
        meta: {public: true, layout: 'standard'}
    },
    {
        path: '/adds/:id/edit',
        name: 'adds.edit',
        component: require('./components/editAddComponent').default,
        meta: {public: true, layout: 'standard'}
    },
    {
        path: '/adds/:id/show',
        name: 'adds.show',
        component: require('./components/showAddComponent').default,
        meta: {public: true, layout: 'standard'}
    },
    {
        path: '/messages',
        name: 'messages.main',
        component: require('./components/MessageComponent').default,
        meta: {public: true, layout: 'standard'}
    }
]

const router = new VueRouter({
    routes,
    linkActiveClass: 'is-active',
});

router.beforeEach((to, from, next) =>
{
    const currentUser = store.state.current_user;
    const authRequired = !to.matched.some(record => record.meta.public);
    const loggedIn = store.getters['authIsAuth'];
    const mustBeAdmin = to.matched.some(record => record.meta.admin);

    if (authRequired)
    {
        if(!loggedIn) {
            return next({ name: 'users.login' });
        }
        if(mustBeAdmin && currentUser.role != 'admin') {
            return next({ name: 'users.login' });
        }
    }

    next();
});

export default router;
