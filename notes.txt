tues 22/9: fixed the issue with auth not working, continued writing the components roughly. 
for thurs 24/9: the getters in the store are not correct, the show add component is not displaying at all, somehow the adds array is one object and the add getter should return the add you want, write different logic.

5/11/2020:
i have added a bunch of dev commenting, to make it a bit more readable, possibly too much, but better too much then too little, for my own sake.
added all the bid stuff to the store, pretty much all actions are finished and working, same with mutations. something is going wrong with the getters with the component for the bids, not sure what.
checked the controller for bids and made some additional dev comments. next up: get something on the screen dangit!

17/11/2020:
mogguh. 1st order of business: my bids components. 
fixed the getters in the store for adds and bids per user. 
made it so the user_id at creating adds and bids is no longer hard coded and stays dynamic.
the progress still feels slow, despite accomplishing more in one morning then in 2 weeks time.
i've finally started working on categorizing adds. i can not seem to access them from a regular pull from the database, so how will i get the categories attached to the adds?
use laravel loading, try laravel tinker with the model.

19/11/2020:
started today with looking at the laravel documentation to see how to call the relation of add and category.
after a minor setback i managed to find the right values and make it work, however it seems that i need to find a way to check if an element is in an array or not.
after this it should be relatively smooth sailing. not much for a day's work, but progress is progress. saving the rest for next week, baibai!

24/11/2020:
good murning. finished the issues i had last week, much easier then i had thought. just used a .includes() method to see if the add has already been added to the array and if so, it wont do it again.
also quickly fixed that after logging in, it will push / to the router, redirecting you to the homepage.
sprint finally finished, this took way too much time, hopefully i can make good progress on the next sprint today and thursday.
today i also realized i have not added the correct validation yet, so routes that should be private are accessible without being logged in, so that needs to be fixed at some point.
see ya thursday, finished tasks today.

26/11/2020:
mjörning. *skipped writing notes this day*

01/12/2020:
pagination is almost done, time to finish it. for the next part i (might) have to change my migrations and add a postal code and adress to the adds.
for thursday: check how to import csv files in laravel properly. possibly fix migration/seeder.

03/12/2020:
ohayo! another cursed thursday today. i realized it is most likely me speaking it to life because it is in my head, but have not thought of a fix yet.
gotta get my head together for next week and be positive through the weekend. see ya next week.
sidenote for next week:
1. see if we need a controller and model for the new table.
2. how will we define the relations with this new table (similar to categories?).
3. try to come up with a way to keep my attention where it needs to be.

15/12/2020: 
nearly the end of a pretty strange and nasty year, twas too late once i realized how much time had passed by without realizing.
either way, おはよ. 
today we start going down the rabbit hole of postal codes, let's go! 
i made a controller to show the index and show methods for the new table (i believe all i need is to read the information in there, not actually alter anything).
i made a model for the controller, but i feel like it may not be needed after all, good to have it for now i guess. next steps:
- accept and store the information from the database somewhere, processing part will come later (store.js)
- rest of the work should actually be store.js stuff mostly if i am thinking correctly, with only minor component changes.
- i am uncertain how to proceed with the actual conversion of latitude and longitude to physical space, ultimately getting the distance between (gonna google some stuff and check back in 30 mins to an hour -11:30-)
get data, put in store (maybe module? maybe state held?)
managed to make a decent dent in this subject, despite my tired mental state, quite chuffed with today actually. i have an hour and a half left and starting to slow down, but i did stuff.
last steps: -set up the store stuff, maybe load it in with users? 
bugs needing fixing: - the collection isnt working, either the table can not be migrated (at migrations) or the collection isnt functioning as intended. -- fixed

12/01/2021:
i made a function that works, but it doesnt quite do everything i want it to do yet. finishing touches on thurs.
it also seems to only loop once instead of continuing on through every add. i believe the next step is to see if i can perhaps assign it to a value somewhere and see if i get multiple results that way. 
for next thursday: take a good look at what you get back from the code and check it against real examples (using google or some other project). 

19/01/2021:
postal codes are finished, jsut needs some polishing that is easier to do once everything is set. 
the websocket stuff is confusing, but i understand the basics. might have to touch up a bit tomorrow because for some reasont he serve command is not working and i am uncertain why.