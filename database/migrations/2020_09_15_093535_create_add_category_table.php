<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_category', function (Blueprint $table) {
            $table->unsignedBigInteger('add_id');
            $table->unsignedBigInteger('category_id');
            $table->primary(['add_id', 'category_id']);
            $table->foreign('add_id')->references('id')->on('adds')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_category');
    }
}
