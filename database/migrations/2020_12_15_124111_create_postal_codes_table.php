<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostalCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postal_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('postcode');
            $table->string('woonplaats');
            $table->string('alternatieve_schrijfwijzen');
            $table->string('gemeente');
            $table->string('provincie');
            $table->string('netnummer');
            $table->float('latitude', 8, 2);
            $table->float('longitude', 8, 2);
            $table->string('soort');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postal_codes');
    }
}
