<?php

use App\Category;
use App\Bid;
use App\User;
use App\Add;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('categories')->insert([
            'title' => 'uwu'
        ]);
        DB::table('categories')->insert([
            'title' => 'more uwu'
        ]);
        DB::table('categories')->insert([
            'title' => 'even more uwu'
        ]);
        DB::table('categories')->insert([
            'title' => 'all the uwu'
        ]);
        DB::table('users')->insert([
            'name' => 'test test',
            'email' => 'test@test.com',
            'username' => 'test123',
            'password' => bcrypt('test'),
            'postalcode_id' => 2
        ]);
        // DB::table('postal_codes')->delete();
        // $statement = "ALTER TABLE postal_codes AUTO_INCREMENT = 1;";
        // DB::unprepared($statement);


    }
}
